﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DataModel
{
    /// <summary>
    /// Единица сценария, описывающая элемент, его целевое состояние и порядок выполнения
    /// </summary>
    public class Task
    {
        public List<TaskElement> TaskElements { get; set; } = new List<TaskElement>();
    }
}
